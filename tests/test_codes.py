
import json
from pathlib import Path
import numpy as np
import tfs
from cpymad.madx import Madx

import xtrack as xt
import xpart as xp
import xobjects as xo

import pytest
import toolkit.cpymad_utils as cpu

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json", encoding='utf-8'))

PLANES = ['x', 'y']
SEED = 1

ERROR_DICT={
    'no_errors':{
        'Reference_radius': 0.01,
        'S[DF].*':{
        'dX_rand':0e-6,
        },
    },
    'alignment':{
        'Reference_radius': 0.01,
        'S[DF].*':{
        'dX_syst':1e-5,
        'dX_rand':7e-6,
        'dY_rand':20e-6,
        },
    },
    'fielderror':{
        'Reference_radius': 0.01,
        'B1.*':{
        'b3_syst':1e-4,
        'b3_rand':5e-4,
        },
    },
}

def abs_difference(reference,value):
    return np.abs(value-reference)

def rel_difference(reference,value):
    return abs_difference(reference,value)/np.abs(reference)

@pytest.mark.parametrize('errors', ['no_errors', 'alignment', 'fielderror'])
def test_compare_thin_twiss(operation_mode, errors, tmp_path):

    with Madx(command_log=f"log_{errors}.madx", cwd=tmp_path) as madx:
        cpu.set_up_lattice(madx,
                           REFERENCE_FILE[operation_mode],
                           str(REPOSITORY_TOP_LEVEL/'lattices'/operation_mode/f"fccee_{operation_mode}.seq"))
        cpu.slice_lattice(madx)

        cpu.create_and_load_error(madx, ERROR_DICT[errors], SEED, tmp_path)
        cpu.run_4d_twiss(madx, str(tmp_path/f'check_twiss_{errors}.tfs'))
        cpymad_df = tfs.read(str(tmp_path/f'check_twiss_{errors}.tfs'))

        madx.input('SELECT, FLAG=ERROR, FULL;')
        madx.input('ESAVE, FILE="err.tfs";')

        line = xt.Line.from_madx_sequence(madx.sequence['FCCEE_P_RING'],
                                    apply_madx_errors=True,
                                    deferred_expressions=True,
                                    install_apertures=False)
    context = xo.ContextCpu()
    ref_particle = xp.Particles(mass0=xp.ELECTRON_MASS_EV,
                                q0=1,
                                p0c=REFERENCE_FILE[operation_mode]['ENERGY']*10**9,
                                x=0,
                                y=0)
    line.particle_ref = ref_particle
    line.build_tracker(_context=context)
    line.config.XTRACK_USE_EXACT_DRIFTS = True
    line.vars['voltca1']=0.
    line.vars['voltca2']=0.
    twiss = line.twiss(method="4d")
    xsuite_df = twiss.to_pandas()
    xsuite_df.to_csv(str(tmp_path/f'twiss_xsuite_{errors}.csv'))

    with open(str(tmp_path/f'tunes_xsuite_{errors}.csv'), 'w') as fp:
        for plane in PLANES:
            fp.write(f"Q{plane}: {getattr(twiss, f'q{plane}')} \n")
            fp.write(f"dQ{plane}: {getattr(twiss, f'dq{plane}')} \n")

    for plane in PLANES:

        mad_x_beta = cpymad_df.iloc[-1][f'BET{plane.upper()}']
        xsuite_beta = xsuite_df.iloc[-1][f'bet{plane}']
        mad_x_tune = cpymad_df.headers[f'Q{"1" if plane == "x" else "2"}']
        xsuite_tune = getattr(twiss, f'q{plane}')
        mad_x_chroma = cpymad_df.headers[f'DQ{"1" if plane == "x" else "2"}']
        xsuite_chroma = getattr(twiss, f'dq{plane}')

        assert rel_difference(mad_x_beta, xsuite_beta) < 0.01, f'Beta{plane} is off. MAD-X:{mad_x_beta} X-Suite: {xsuite_beta}'
        assert rel_difference(mad_x_tune, xsuite_tune) < 0.01, f'Tune{plane} is off. MAD-X:{mad_x_tune} X-Suite: {xsuite_tune}'
        assert abs_difference(mad_x_chroma, xsuite_chroma) < 1.0, f'Chroma{plane} is off. MAD-X:{mad_x_chroma} X-Suite: {xsuite_chroma}'
