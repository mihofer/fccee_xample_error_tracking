
import toolkit.create_error_table as cet


def set_up_lattice(mad_instance, reference_parameter, seq_path):

    mad_instance.call(seq_path)
    mad_instance.beam(
                particle='electron',
                npart=reference_parameter['BUNCH_POPULATION'],
                kbunch=reference_parameter['BUNCHES'],
                pc=reference_parameter['ENERGY'],
                radiate=False,
                bv=1,
                ex=reference_parameter['EMITTANCE_X'],
                ey=reference_parameter['EMITTANCE_Y'])
    mad_instance.use('FCCEE_P_RING')


def match_chromaticity(mad_instance, chromas, sextupole_script_path):
    mad_instance.call(sextupole_script_path)
    mad_instance.input(
"""VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

VOLTCA1 = 0;
VOLTCA2 = 0;""")
    mad_instance.input(
f"""MATCH, SEQUENCE=FCCEE_P_RING, CHROM;
VARY, NAME=KN_SF;
VARY, NAME=KN_SD;

GLOBAL, DQ1={chromas['QX']};
GLOBAL, DQ2={chromas['QY']};

LMDIF, CALLS=10000;
ENDMATCH;
""")
    
    mad_instance.input(
"""VOLTCA1 = VOLTCA1SAVE;
VOLTCA2 = VOLTCA2SAVE;""")


def taper_lattice(mad_instance):
    mad_instance.input(
"""
VOLTCA1 = 0;
VOLTCA2 = 0;
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=FCCEE_P_RING;
TWISS; 
VOLTCA1 = VOLTCA1SAVE;
VOLTCA2 = VOLTCA2SAVE;

BEAM, RADIATE=TRUE;

MATCH, sequence=FCCEE_P_RING, BETA0 = B.IP, tapering;
VARY, NAME=LAGCA1, step=1.0E-7;
VARY, NAME=LAGCA2, step=1.0E-7;
CONSTRAINT, SEQUENCE=FCCEE_P_RING, RANGE=#e, PT=0.0;
JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
ENDMATCH;

USE, SEQUENCE = FCCEE_P_RING;

TWISS, TAPERING;
""")


def slice_lattice(mad_instance):

    mad_instance.input(
"""VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

VOLTCA1 = 0;
VOLTCA2 = 0;""")

    mad_instance.input(
"""TWISS;
THICK_QX = TABLE(SUMM, Q1);
THICK_QY = TABLE(SUMM, Q2);""")
    
    mad_instance.input(
"""SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
SELECT, FLAG=makethin, CLASS=rbend, SLICE = 4;
SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 4;
SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 4;

SELECT, FLAG=makethin, PATTERN="^QF.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QD.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QFG.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QDG.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QL.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QS.*", SLICE=5;
SELECT, FLAG=makethin, PATTERN="^QB.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QG.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QH.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QI.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QR.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QU.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QY.*", SLICE=10;
SELECT, FLAG=makethin, PATTERN="^QA.*", SLICE=50;
SELECT, FLAG=makethin, PATTERN="^QC.*", SLICE=50;
SELECT, FLAG=makethin, PATTERN="^SY.*", SLICE=20;
        
MAKETHIN, SEQUENCE=FCCEE_P_RING, STYLE=TEAPOT, MAKEDIPEDGE=True;""")
    mad_instance.use('FCCEE_P_RING')

    mad_instance.input(
"""MATCH, SEQUENCE=FCCEE_P_RING;
VARY, NAME=K1QF4;
VARY, NAME=K1QF2;
VARY, NAME=K1QD3;
VARY, NAME=K1QD1;

GLOBAL, Q1=THICK_QX;
GLOBAL, Q2=THICK_QY;

LMDIF, CALLS=10000;
JACOBIAN, CALLS=10000;
ENDMATCH;""")
    
    mad_instance.input(
"""VOLTCA1 = VOLTCA1SAVE;
VOLTCA2 = VOLTCA2SAVE;""")

def run_4d_twiss(mad_instance, filename):

    mad_instance.input(
"""VOLTCA1SAVE = VOLTCA1; 
VOLTCA2SAVE = VOLTCA2; 

VOLTCA1 = 0;
VOLTCA2 = 0;""")

    mad_instance.twiss(sequence='FCCEE_P_RING', chrom=True, file=filename)

    mad_instance.input(
"""VOLTCA1 = VOLTCA1SAVE;
VOLTCA2 = VOLTCA2SAVE;""")


def run_6d_twiss(mad_instance, filename):

    mad_instance.twiss(sequence='FCCEE_P_RING', chrom=True, file=filename, tapering=True)


def create_and_load_error(mad_instance, error_dict, seed, cwd):

    twiss_path = str(cwd/'test.tfs')
    error_path = str(cwd/f'errors_{seed}.tfs')

    run_4d_twiss(mad_instance, twiss_path)
    mad_instance.input("SELECT, FLAG=error, CLEAR;")
    cet.main(
        errors_dict=error_dict,
        twiss_file=twiss_path,
        seed=seed,
        outputfile=error_path
        )

    mad_instance.input(f"READTABLE, file='{error_path}', table=errtab;")
    mad_instance.input("SETERR, TABLE=errtab;")
